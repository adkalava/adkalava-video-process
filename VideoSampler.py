#!/usr/bin/python3
# Module used to sample frames from video for Adkalava
# Author: Jeff Wen

import ffmpy
import math
import os
import json
import hashlib
import datetime
import urllib.request
import pymongo
from pymongo import MongoClient

if __name__=='__main__':
    import argparse

class VideoSampler:
    """Adkalava video sampler
    """
    def __init__(
            self,
            video_path,
            dest_path,
            fps=0,
            video_url_prefix=None,
            dest_url_prefix=None,
            recg_server=None,
            recg_combiner_id=None,
            recg_threshold=0.3,
            mongo_client=None,
            ad_interval=40000,
            ad_duration=30000
    ):
        """Initialization.
        Args:
        video_path (string): full path of video to process
        dest_path (string): sampling pictures go self path
        fps (float): fps to sampling frames
        video_url_prefix (string): video url prefix
        dest_url_prefix (string): dest frame resource url prefix
        recg_server (string): url to recognization server
        recg_combiner_id (list): list of combiner ids for recg
        recg_threshold (float): threshold to keep the frame
        mongo_client (pymongo.MongoClient): mongo client
        ad_interval (int): ad process time unit, in ms
        ad_duration (int): defines how long a single ad lives
        """
        if not os.path.isfile(video_path):
            raise FileExistsError("Video file does not exist")
        if not os.path.exists(dest_path):
            raise FileExistsError("Dest path(" + dest_path + 
                ") does not exist")
        self.video_path = video_path
        self._video_file_name = os.path.basename(self.video_path)
        self.dest_path = dest_path
        if mongo_client:
            self.mongo_client = mongo_client
            self.db_cleanup_flag = False
        else:
            self.mongo_client = MongoClient("127.0.0.1", 27017)
            self.db_cleanup_flag = True
        self.fps = fps
        self.ad_interval = ad_interval
        self.ad_duration = ad_duration
        self.frames = []
        self.set_recg_server(recg_server, recg_combiner_id)
        self.video_info = None
        self.set_dest_url_prefix(dest_url_prefix)
        self.set_video_url(video_url_prefix)
        self.md5 = hashlib.md5()
        self.md5.update(self.video_url.encode('utf-8'))
        self.video_id = self.md5.hexdigest()
        self._fixed_num_str = None
        self.video_title = os.path.splitext(
            os.path.basename(self.video_path)
        )[0]
        self._frame_file_prefix = dest_path + '/' + self.video_title
        self.frames_length = 0
        self.sampling_status = False
        self.frames_ready = False
        self.frames_saved = False
        self.recg_threshold = recg_threshold

    def __enter__(self):
        return self

    def __exit__(self):
        if self.db_cleanup_flag:
            self.mongo_client.close()

    def clean_up(self):
        if self.db_cleanup_flag:
            self.mongo_client.close()

    def set_dest_url_prefix(self, dest_url_prefix):
        """Set destination frame url prefix
        NOTE: must end with '/'
        """
        self._dest_url_prefix = dest_url_prefix

    def set_video_url(self, video_url_prefix):
        """Set destination frame url prefix
        NOTE: must end with '/'
        """
        self._video_url_prefix = video_url_prefix
        self.video_url = video_url_prefix + self._video_file_name

    def set_recg_server(self, recg_server, recg_combiner_id):
        """Set up recognization server
        Args:
        recg_server (string): url to recognization server
        recg_combiner_id (list): list of combiner ids for recg
        """
        self.recg_server = recg_server
        self.recg_combiner_id = recg_combiner_id or ["5999", "6000", "6001"]

    def get_video_info(self, global_options=None):
        """Get video info by ffprobe.
        Self method also save the video info, calc and save frames number, 
        and construct the self.frames list.

        Args:
        global_options (string): global options for ffprobe, 
            you can use this to specify info format etc.

        Return:
            the video info dict
        """
        print("get image info by ffprobe")
        default_global_options = '-v error -print_format json -select_streams v:0 '
        default_global_options += '-show_entries stream=duration,width,height'
        global_options = global_options or default_global_options
        fp = ffmpy.FFprobe(
            inputs={video_path: None},
            global_options=global_options
        )
        video_info = json.loads(fp.run().decode('utf-8')).get('streams')[0]
        if video_info:
            video_info['duration'] = float(video_info['duration'])
        self.video_info = video_info
        self._frames_list_construct()
        return video_info

    def _frames_list_construct(self):
        if self.frames_ready:
            return
        begin_frame = (self.video_info['duration'] % (1 / self.fps)) * self.fps * 2
        self.frames_length = int(self.video_info['duration'] * self.fps)
        self.frames_length += int(begin_frame)
        file_num_width = math.floor(math.log10(self.frames_length)) + 1
        self._fixed_num_width = file_num_width
        self._fixed_num_str = '_%0' + str(file_num_width) +'d.png'
        file_name = self._dest_url_prefix + self.video_title + self._fixed_num_str
        self.frames = [{
            'path': file_name % (i + 1),
            'offset': (i + 0.5) / self.fps * 1000,
            'duration': self.ad_duration
        } for i in range(0, self.frames_length)]

    def sample(self):
        if self.frames_ready:
            return
        if not self.video_info:
            self.get_video_info()
        if not self._frames_exist():
            print("sampling video...")
            file_name = self._frame_file_prefix + self._fixed_num_str
            ff = ffmpy.FFmpeg(
                inputs = {self.video_path: None},
                outputs = {file_name: '-vf fps=' + str(fps)}
            )
            ff.run()
        self.sampling_status = True

    def send_to_remote(self, remote_server):
        """Send frames to remote server, leave procedures to the remote server.
        """
        if not self.sampling_status:
            self.sample()
        post_data = {
            'videoId': self.video_id,
            'videoURL': self.video_url,
            'width': self.video_info.get('width'),
            'height': self.video_info.get('height'),
            'frames': self.frames,
            'duration': int(self.video_info.get('duration') * 1000),
            'adInterval': self.ad_interval
        }
        params = json.dumps(post_data).encode('utf-8')
        headers = {
            'Content-Type': 'application/json'
        }
        def send_req():
            req = urllib.request.Request(remote_server, data=params, headers=headers)
            with urllib.request.urlopen(req) as response:
                response = json.loads(response.read().decode('utf-8'))
            return response
        print(send_req())

    def send_recg_request(self, frame_index, timeout=2):
        """Send indexed frame to recognize, to dertermine keep or not
        Args:
        frame_index (int): index of frame
        timeout (int): post request timeout

        Return:
            return recognized data
        """
        if not self.frames[frame_index]:
            return None
        frame = self.frames[frame_index]
        print(('[recg]: %0' + str(self._fixed_num_width) + 'd') % 
            frame_index, frame['path'])
        post_data = {
            'combiner_id': self.recg_combiner_id,
            'image_url': frame['path'],
            'timeout': timeout
        }
        params = json.dumps(post_data).encode('utf-8')
        headers = {
            'Content-Type': 'application/json'
        }
        def send_req():
            req = urllib.request.Request(self.recg_server, data=params, headers=headers)
            with urllib.request.urlopen(req) as response:
                response = json.loads(response.read().decode('utf-8'))
            return response
        response = send_req()
        while (response["errno"] == 2):
            response = send_req()
        recg_result = response.get('data')
        frame['should_keep'] = False
        if not recg_result:
            frame['should_keep'] = False
            return
        if recg_result.get('face') or recg_result.get('detect'):
            frame['should_keep'] = True
            for scene in recg_result.get('scene'):
                if scene['score'] > self.recg_threshold:
                    frame['should_keep'] = True
        return response

    def send_recg_request_all(self, timeout=2):
        """Send all frames to recgonization server to determine keep or not
        """
        if self.frames_ready:
            return
        if not self.sampling_status:
            self.sample()
        responses = []
        for i in range(0, self.frames_length):
            responses.append(self.send_recg_request(i))
        return responses

    def _frames_exist(self):
        """Check if ALL corresponding sampling pictures already exist
        Return:
            bool
        """
        all_frames_exist = True
        if not self.video_info:
            self.get_video_info()
        file_name = self._frame_file_prefix + self._fixed_num_str
        for i in range(1, self.frames_length + 1):
            if not os.path.isfile(file_name % i):
                all_frames_exist = False
                break
        if all_frames_exist:
            print("this video has already been sampled.")
        return all_frames_exist

    def _frame_interval_validity_confirm(self):
        """Frame interval validity confirm
        """
        if self.frames_ready:
            return
        frames = sorted(self.frames, key=lambda frame: frame['offset'])
        timepoint = frames[len(frames) - 1]['offset']
        for i in range(len(frames) - 2, -1, -1):
            if frames[i]['offset'] + ad_interval > timepoint:
                del frames[i]
                continue
            timepoint = frames[i]['offset']
        self.frames = frames
        print("ad interval ensured.")
        self.frames_ready = True

    def save(self):
        """Save frames' info into database
        """
        if self.frames_saved:
            print('already saved')
            return
        if not self.frames_ready:
            self.send_recg_request_all()
        self._frame_interval_validity_confirm()
        db = self.mongo_client.yunshitu
        cursor = db.adk_video_info.find_one({'videoId': self.video_id})
        if (cursor):
            print('this video exists in db.')
            self.frames_saved = True
            return
        frames = [frame for frame in self.frames if frame['should_keep']]
        for frame in frames:
            del frame['should_keep']
        if len(frames):
            doc = {
                'videoId': self.video_id,
                'width': self.video_info.get('width'),
                'height': self.video_info.get('height'),
                'videoURL': self.video_url,
                'frames': frames,
                'createdAt': datetime.datetime.utcnow()
            }
            try:
                print('insert into db...')
                result = db.adk_video_info.insert_one(doc)
            except pymongo.errors.DuplicateKeyError:
                print('this video has already been procceed')
        else:
            print('no usable frame!')
        self.frames_saved = True

if __name__=='__main__':
    db_host = '192.168.1.122'
    db_port = 27017
    frames_path = '/adkalava/stream-server/dist/video-frames/'
    ad_interval = 40 * 1000
    ad_duration = 30 * 1000

    parser = argparse.ArgumentParser(description='Adkalava Video Process Script')
    parser.add_argument(dest='video_path', metavar='VIDEO_FILE',
                        help='video file path')
    parser.add_argument('-d', '--db-host', dest='db_host',
                        help='mongo databse host')
    parser.add_argument('-p', '--db-port', type=int, dest='db_port',
                        help='mongo databas port')
    parser.add_argument('-f', '--frames-path', dest='frames_path',
                        help='video frames save path')
    parser.add_argument('-fps', dest='fps', required=True,
                        help='fps to retrive video frames')
    parser.add_argument('-s', '--single-mode', action='store_true',
                        help='capture single frame')
    parser.add_argument('-o', '--offset', dest='offset', type=int,
                        help='time offset used to capture single frame')
    parser.add_argument('-i', '--ad-interval', type=int, dest='ad_interval',
                        help='time unit for ad process, in ms')
    parser.add_argument('-r', '--duration', type=int, dest='ad_duration',
                        help='duration of ads')
    args = parser.parse_args()
    video_path = args.video_path
    db_host = args.db_host or db_host
    db_port = args.db_port or db_port
    frames_path = args.frames_path or frames_path
    ad_interval = args.ad_interval or ad_interval
    ad_duration = args.ad_duration or ad_duration
    fps = eval(args.fps, {'__builtins__': None})

    with MongoClient(db_host, db_port) as client:
        vs = VideoSampler(
            video_path,
            frames_path,
            fps,
            dest_url_prefix='http://v.adkalava.cn/video-frames/',
            video_url_prefix='http://v.adkalava.cn/',
            recg_server='http://imgserver.yunshitu.cn/v1/dispatcher',
            ad_interval=ad_interval,
            ad_duration=ad_duration,
            mongo_client=client
            )
        vs.send_to_remote('http://vt.adkalava.cn/task')
